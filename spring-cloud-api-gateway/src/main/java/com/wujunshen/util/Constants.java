package com.wujunshen.util;

public class Constants {
    public static final String NULL_DATA = "";
    public static final String BEARER = "bearer";

    private Constants() {
    }
}